﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MoneyTracker.Domain.Data;
using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.UserDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Domain.Services.Implementations
{
    public class UserService : IUserService
    {
        private ApplicationDbContext DbContext { get; }
        private IMapper Mapper { get; }

        public UserService(ApplicationDbContext dbContext, IMapper mapper)
        {
            DbContext = dbContext;
            Mapper = mapper;
        }

        public async Task<UserDto> GetUserByIdAsync(Guid id)
        {
            var user = await GetUserAsync(id);
            return Mapper.Map<UserDto>(user);
        }

        public async Task RegisterUserAsync(CreateUserDto newUser)
        {
            if (await DbContext.Users.FirstOrDefaultAsync(u =>
                u.Email == newUser.Email && !u.IsDeleted) != null)
            {
                throw new Exception($"User with email \"{newUser.Email}\" already exist");
            }

            await DbContext.Users.AddAsync(Mapper.Map<User>(newUser));
            await DbContext.SaveChangesAsync();
        }

        public async Task EditUserAsync(EditUserDto editUserDto)
        {
            var user = await GetUserAsync(editUserDto.Id!.Value);
            user.Email = editUserDto.Email;
            DbContext.Users.Update(user);
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteUserByIdAsync(Guid id)
        {
            var user = await GetUserAsync(id);
            DbContext.Users.Remove(user);
            await DbContext.SaveChangesAsync();
        }

        public async Task<User> GetUserAsync(Guid id)
        {
            return await DbContext.Users
                .FirstOrDefaultAsync(u => u.Id == id && !u.IsDeleted)
                    ?? throw new KeyNotFoundException($"User with id = {id} does not found");
        }
    }
}