﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MoneyTracker.Domain.Data;
using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.AccountDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Domain.Services.Implementations
{
    public class AccountService : IAccountService
    {
        private ApplicationDbContext DbContext { get; }
        private IMapper Mapper { get; }
        private IUserService UserService { get; }

        public AccountService(ApplicationDbContext dbContext, 
            IMapper mapper, IUserService userService)
        {
            DbContext = dbContext;
            Mapper = mapper;
            UserService = userService;
        }

        public async Task<AccountDto> GetAccountByIdAsync(Guid id)
        {
            var account = await GetAccountAsync(id);
            return Mapper.Map<AccountDto>(account);
        }

        public async Task AddAccountAsync(CreateAccountDto newAccount)
        {
            if (await DbContext.Accounts.FirstOrDefaultAsync(a =>
                a.Title == newAccount.Title
                && a.Owner.Id == newAccount.OwnerId
                && !a.IsDeleted) != null)
            {
                throw new Exception($"User already has account with title \"{newAccount.Title}\"");
            }

            var mapped = Mapper.Map<Account>(newAccount);
            mapped.Owner = await UserService.GetUserAsync(newAccount.OwnerId!.Value);
            await DbContext.Accounts.AddAsync(mapped);
            await DbContext.SaveChangesAsync();
        }

        public async Task EditAccountAsync(EditAccountDto editAccountDto)
        {
            var account = await GetAccountAsync(editAccountDto.Id!.Value);
            account.Title = editAccountDto.Title ?? account.Title;
            account.Type = editAccountDto.Type ?? account.Type;
            account.Currency = editAccountDto.Currency ?? account.Currency;
            account.CurrentBalance = editAccountDto.CurrentBalance ?? account.CurrentBalance;
            DbContext.Accounts.Update(account);
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteAccountByIdAsync(Guid id)
        {
            var account = await GetAccountAsync(id);
            DbContext.Accounts.Remove(account);
            await DbContext.SaveChangesAsync();
        }

        public async Task<Account> GetAccountAsync(Guid id)
        {
            return await DbContext.Accounts
                .Include(a => a.Owner)
                .FirstOrDefaultAsync(a => a.Id == id && !a.IsDeleted)
                    ?? throw new KeyNotFoundException($"Account with id = {id} does not found");
        }
    }
}