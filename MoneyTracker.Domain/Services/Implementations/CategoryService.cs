﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MoneyTracker.Domain.Data;
using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.CategoryDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Domain.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private ApplicationDbContext DbContext { get; }
        private IMapper Mapper { get; }
        private IUserService UserService { get; }

        public CategoryService(ApplicationDbContext dbContext, 
            IMapper mapper, IUserService userService)
        {
            DbContext = dbContext;
            Mapper = mapper;
            UserService = userService;
        }

        public async Task<CategoryDto> GetCategoryByIdAsync(Guid id)
        {
            var сategory = await GetCategoryAsync(id);
            return Mapper.Map<CategoryDto>(сategory);
        }

        public async Task AddCategoryAsync(CreateCategoryDto newCategory)
        {
            if (await DbContext.Categories.FirstOrDefaultAsync(c =>
                c.Title == newCategory.Title
                && c.Owner.Id == newCategory.OwnerId
                && !c.IsDeleted) != null)
            {
                throw new Exception($"User already has category with title \"{newCategory.Title}\"");
            }

            ValidateCategory(newCategory.IsParent!.Value, newCategory.ParentCategoryId);

            var mapped = Mapper.Map<Category>(newCategory);
            mapped.Owner = await UserService.GetUserAsync(newCategory.OwnerId!.Value);
            mapped.ParentCategory = newCategory.ParentCategoryId != null
                ? await GetParentCategoryAsync(newCategory.ParentCategoryId.Value)
                : null;
            await DbContext.Categories.AddAsync(mapped);
            await DbContext.SaveChangesAsync();
        }

        public async Task EditCategoryAsync(EditCategoryDto editCategoryDto)
        {
            var category = await GetCategoryAsync(editCategoryDto.Id!.Value);
            category.Title = editCategoryDto.Title ?? category.Title;
            category.ExpenseType = editCategoryDto.ExpenseType ?? category.ExpenseType;

            if (editCategoryDto.CategoryType != null && editCategoryDto.CategoryType != category.CategoryType)
            {
                if (category.Transactions != null && category.Transactions.Count > 0)
                {
                    throw new Exception("This category is already used in transactions, you cannot change its type");
                }
                category.CategoryType = editCategoryDto.CategoryType.Value;
            }

            if (editCategoryDto.IsParent != null)
            {
                ValidateCategory(editCategoryDto.IsParent.Value, editCategoryDto.ParentCategoryId);
                category.IsParent = editCategoryDto.IsParent.Value;
                if (editCategoryDto.ParentCategoryId != null)
                {
                    if (category.ChildCategories != null && category.ChildCategories.Count > 0
                        && editCategoryDto.IsParent.Value == false)
                    {
                        throw new Exception($"Can't make a category a child category. This category has child categories");
                    }
                    category.ParentCategory = await GetParentCategoryAsync(editCategoryDto.ParentCategoryId.Value);
                }
            }
            else if (editCategoryDto.ParentCategoryId != null)
            {
                if (category.IsParent)
                {
                    throw new Exception($"Parent category cannot have a parent");
                }
                category.ParentCategory = await GetParentCategoryAsync(editCategoryDto.ParentCategoryId.Value);
            }

            DbContext.Categories.Update(category);
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteCategoryByIdAsync(Guid id)
        {
            var category = await GetCategoryAsync(id);
            if (category.ChildCategories != null)
            {
                DbContext.Categories.RemoveRange(category.ChildCategories);
            }
            DbContext.Categories.Remove(category);
            await DbContext.SaveChangesAsync();
        }

        public async Task<Category> GetCategoryAsync(Guid id)
        {
            return await DbContext.Categories
                .Include(c => c.Owner)
                .Include(c => c.ParentCategory)
                .Include(c => c.Transactions)
                .Include(c => c.ChildCategories.Where(c => !c.IsDeleted))
                .FirstOrDefaultAsync(c => c.Id == id && !c.IsDeleted)
                    ?? throw new KeyNotFoundException($"Category with id = {id} does not found");
        }

        private void ValidateCategory(bool isParent, Guid? parentCategoryId)
        {
            if (isParent && parentCategoryId != null)
            {
                throw new Exception($"Parent category cannot have a parent");
            }
            if (!isParent && parentCategoryId == null)
            {
                throw new Exception($"Parent category not specified");
            }
        }

        private async Task<Category> GetParentCategoryAsync(Guid? id)
        {
            return await DbContext.Categories
                .FirstOrDefaultAsync(c => c.Id == id && !c.IsDeleted)
                    ?? throw new Exception($"Parent category not found");
        }
    }
}