﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MoneyTracker.Domain.Data;
using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.Models.Enums;
using MoneyTracker.Domain.Data.ModelsDto.TransactionDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Domain.Services.Implementations
{
    public class TransactionService : ITransactionService
    {
        private ApplicationDbContext DbContext { get; }
        private IMapper Mapper { get; }
        private IAccountService AccountService { get; }
        private ICategoryService CategoryService { get; }

        public TransactionService(ApplicationDbContext dbContext, IMapper mapper, 
            IAccountService accountService, ICategoryService categoryService)
        {
            DbContext = dbContext;
            Mapper = mapper;
            AccountService = accountService;
            CategoryService = categoryService;
        }

        public async Task<TransactionDto> GetTransactionByIdAsync(Guid id)
        {
            var transaction = await GetTransactionAsync(id);
            return Mapper.Map<TransactionDto>(transaction);
        }

        public async Task AddTransactionAsync(CreateTransactionDto newTransaction)
        {
            var account = await AccountService.GetAccountAsync(newTransaction.AccountId!.Value);
            account.CurrentBalance = newTransaction.Type == TransactionType.Expense
                ? account.CurrentBalance - newTransaction.TransactionAmount!.Value
                : account.CurrentBalance + newTransaction.TransactionAmount!.Value;

            var mapped = Mapper.Map<Transaction>(newTransaction);
            mapped.Account = account;
            mapped.Category = newTransaction.CategoryId != null
                ? await CategoryService.GetCategoryAsync(newTransaction.CategoryId.Value)
                : null;
            await DbContext.Transactions.AddAsync(mapped);
            await DbContext.SaveChangesAsync();
        }

        public async Task EditTransactionAsync(EditTransactionDto editTransactionDto)
        {
            var transaction = await GetTransactionAsync(editTransactionDto.Id!.Value);
            transaction.Category = editTransactionDto.CategoryId != null
                ? await CategoryService.GetCategoryAsync(editTransactionDto.CategoryId.Value)
                : transaction.Category;
            transaction.Date = editTransactionDto.Date ?? transaction.Date;
            transaction.Comment = editTransactionDto.Comment ?? transaction.Comment;

            if (editTransactionDto.AccountId != null && editTransactionDto.AccountId != transaction.Account.Id)
            {
                var account = await AccountService.GetAccountAsync(editTransactionDto.AccountId.Value);

                if (transaction.Type == TransactionType.Expense)
                {
                    transaction.Account.CurrentBalance += transaction.TransactionAmount;
                    account.CurrentBalance -= transaction.TransactionAmount;
                }
                else
                {
                    transaction.Account.CurrentBalance -= transaction.TransactionAmount;
                    account.CurrentBalance += transaction.TransactionAmount;
                }

                transaction.Account = account;
            }

            if (editTransactionDto.Type != null && editTransactionDto.Type != transaction.Type)
            {
                transaction.Account.CurrentBalance = editTransactionDto.Type == TransactionType.Expense
                    ? transaction.Account.CurrentBalance - transaction.TransactionAmount * 2
                    : transaction.Account.CurrentBalance + transaction.TransactionAmount * 2;
                transaction.Type = editTransactionDto.Type.Value;
            }

            if (editTransactionDto.TransactionAmount != null)
            {
                var difference = Math.Abs(transaction.TransactionAmount - editTransactionDto.TransactionAmount.Value);
                transaction.Account.CurrentBalance = transaction.Type == TransactionType.Expense
                    ? transaction.Account.CurrentBalance - difference
                    : transaction.Account.CurrentBalance + difference;
                transaction.TransactionAmount = editTransactionDto.TransactionAmount.Value;
            }

            DbContext.Transactions.Update(transaction);
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteTransactionByIdAsync(Guid id)
        {
            var transaction = await GetTransactionAsync(id);
            transaction.Account.CurrentBalance = transaction.Type == TransactionType.Expense
                ? transaction.Account.CurrentBalance + transaction.TransactionAmount
                : transaction.Account.CurrentBalance - transaction.TransactionAmount;
            DbContext.Transactions.Remove(transaction);
            await DbContext.SaveChangesAsync();
        }

        private async Task<Transaction> GetTransactionAsync(Guid id)
        {
            return await DbContext.Transactions
                .Include(t => t.Account)
                .Include(t => t.Category)
                .FirstOrDefaultAsync(t => t.Id == id && !t.IsDeleted)
                    ?? throw new KeyNotFoundException($"Transaction with id = {id} does not found");
        }
    }
}