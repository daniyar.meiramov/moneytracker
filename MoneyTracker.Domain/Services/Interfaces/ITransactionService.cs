﻿using MoneyTracker.Domain.Data.ModelsDto.TransactionDtos;

namespace MoneyTracker.Domain.Services.Interfaces
{
    public interface ITransactionService
    {
        Task<TransactionDto> GetTransactionByIdAsync(Guid id);
        Task AddTransactionAsync(CreateTransactionDto newTransaction);
        Task EditTransactionAsync(EditTransactionDto editTransactionDto);
        Task DeleteTransactionByIdAsync(Guid id);
    }
}