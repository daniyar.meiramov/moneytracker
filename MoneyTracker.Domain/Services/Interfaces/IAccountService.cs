﻿using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.AccountDtos;

namespace MoneyTracker.Domain.Services.Interfaces
{
    public interface IAccountService
    {
        Task<AccountDto> GetAccountByIdAsync(Guid id);
        Task AddAccountAsync(CreateAccountDto newAccount);
        Task EditAccountAsync(EditAccountDto editAccountDto);
        Task DeleteAccountByIdAsync(Guid id);
        Task<Account> GetAccountAsync(Guid id);
    }
}