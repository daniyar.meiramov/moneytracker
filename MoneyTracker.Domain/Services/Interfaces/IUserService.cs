﻿using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.UserDtos;

namespace MoneyTracker.Domain.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> GetUserByIdAsync(Guid id);
        Task RegisterUserAsync(CreateUserDto newUser);
        Task EditUserAsync(EditUserDto editUserDto);
        Task DeleteUserByIdAsync(Guid id);
        Task<User> GetUserAsync(Guid id);
    }
}