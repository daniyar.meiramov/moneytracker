﻿using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.CategoryDtos;

namespace MoneyTracker.Domain.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryDto> GetCategoryByIdAsync(Guid id);
        Task AddCategoryAsync(CreateCategoryDto newCategory);
        Task EditCategoryAsync(EditCategoryDto editCategoryDto);
        Task DeleteCategoryByIdAsync(Guid id);
        Task<Category> GetCategoryAsync(Guid id);
    }
}