﻿using AutoMapper;
using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.ModelsDto.AccountDtos;
using MoneyTracker.Domain.Data.ModelsDto.CategoryDtos;
using MoneyTracker.Domain.Data.ModelsDto.TransactionDtos;
using MoneyTracker.Domain.Data.ModelsDto.UserDtos;

namespace MoneyTracker.Domain.Configuration
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<CreateUserDto, User>();

            CreateMap<Account, AccountDto>();
            CreateMap<CreateAccountDto, Account>();

            CreateMap<Category, CategoryDto>();
            CreateMap<CreateCategoryDto, Category>();

            CreateMap<Transaction, TransactionDto>();
            CreateMap<CreateTransactionDto, Transaction>();
        }
    }
}