﻿using Microsoft.EntityFrameworkCore;
using MoneyTracker.Domain.Data.Models;

namespace MoneyTracker.Domain.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

        public override int SaveChanges()
        {
            UpdateCreateAndModifyProperties();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken
            = new CancellationToken())
        {
            UpdateCreateAndModifyProperties();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void UpdateCreateAndModifyProperties()
        {
            ChangeTracker.DetectChanges();

            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        if (entry.Entity is BaseEntity trackDeleted)
                        {
                            trackDeleted.ModifyDateTime = DateTime.UtcNow;
                            trackDeleted.IsDeleted = true;
                        }
                        break;
                    case EntityState.Modified:
                        if (entry.Entity is BaseEntity trackModified)
                        {
                            trackModified.ModifyDateTime = DateTime.UtcNow;
                        }
                        break;
                    case EntityState.Added:
                        if (entry.Entity is BaseEntity trackAdded)
                        {
                            trackAdded.CreateDateTime = DateTime.UtcNow;
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}
