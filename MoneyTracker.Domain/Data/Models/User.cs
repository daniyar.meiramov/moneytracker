﻿namespace MoneyTracker.Domain.Data.Models
{
    public class User : BaseEntity
    {
        public string Email { get; set; } = null!;
        public List<Account>? Accounts { get; set; }
        public List<Category>? Categories { get; set; }
    }
}