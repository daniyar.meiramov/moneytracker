﻿using MoneyTracker.Domain.Data.Models.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoneyTracker.Domain.Data.Models
{
    public class Account : BaseEntity
    {
        public User Owner { get; set; } = null!;
        public List<Transaction>? Transactions { get; set; }
        public string Title { get; set; } = null!;
        public AccountType Type { get; set; }
        public Currency Currency { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal CurrentBalance { get; set; }
    }
}