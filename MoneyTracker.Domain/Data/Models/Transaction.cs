﻿using MoneyTracker.Domain.Data.Models.Enums;

namespace MoneyTracker.Domain.Data.Models
{
    public class Transaction : BaseEntity
    {
        public Account Account { get; set; } = null!;
        public Category? Category { get; set; }
        public TransactionType Type { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime Date { get; set; }
        public string? Comment { get; set; }
    }
}