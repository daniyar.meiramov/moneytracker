﻿namespace MoneyTracker.Domain.Data.Models.Enums
{
    public enum TransactionType
    {
        Expense,
        Income
    }
}