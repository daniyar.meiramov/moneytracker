﻿namespace MoneyTracker.Domain.Data.Models.Enums
{
    public enum ExpenseType
    {
        Fixed,
        Flexible
    }
}