﻿namespace MoneyTracker.Domain.Data.Models.Enums
{
    public enum CategoryType
    {
        Expense,
        Income
    }
}