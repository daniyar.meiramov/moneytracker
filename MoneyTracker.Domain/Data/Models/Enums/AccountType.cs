﻿namespace MoneyTracker.Domain.Data.Models.Enums
{
    public enum AccountType
    {
        Card,
        Cash,
        Deposit
    }
}