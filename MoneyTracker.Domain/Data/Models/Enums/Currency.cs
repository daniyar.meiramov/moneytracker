﻿namespace MoneyTracker.Domain.Data.Models.Enums
{
    public enum Currency
    {
        KazakhstaniTenge,
        UnitedStatesDollar,
        RussianRuble
    }
}