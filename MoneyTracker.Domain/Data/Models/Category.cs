﻿using MoneyTracker.Domain.Data.Models.Enums;

namespace MoneyTracker.Domain.Data.Models
{
    public class Category : BaseEntity
    {
        public User Owner { get; set; } = null!;
        public bool IsParent { get; set; }
        public Category? ParentCategory { get; set; }
        public List<Category>? ChildCategories { get; set; }
        public List<Transaction>? Transactions { get; set; }
        public string Title { get; set; } = null!;
        public CategoryType CategoryType { get; set; }
        public ExpenseType ExpenseType { get; set; }
    }
}