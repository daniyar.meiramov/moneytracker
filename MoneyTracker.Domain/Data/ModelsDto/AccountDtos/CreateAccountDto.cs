﻿using MoneyTracker.Domain.Data.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.AccountDtos
{
    public class CreateAccountDto
    {
        [Required]
        public Guid? OwnerId { get; set; }
        [Required]
        public string Title { get; set; } = null!;
        [Required]
        public AccountType? Type { get; set; }
        [Required]
        public Currency? Currency { get; set; }
        public decimal CurrentBalance { get; set; }
    }
}