﻿using MoneyTracker.Domain.Data.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.AccountDtos
{
    public class EditAccountDto
    {
        [Required]
        public Guid? Id { get; set; }
        public string? Title { get; set; }
        public AccountType? Type { get; set; }
        public Currency? Currency { get; set; }
        public decimal? CurrentBalance { get; set; }
    }
}