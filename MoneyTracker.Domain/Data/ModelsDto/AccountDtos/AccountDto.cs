﻿using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.Models.Enums;

namespace MoneyTracker.Domain.Data.ModelsDto.AccountDtos
{
    public class AccountDto
    {
        public Guid OwnerId { get; set; }
        public string OwnerEmail { get; set; } = null!;
        //public List<Transaction>? Transactions { get; set; } // а нужно ли вообще
        public string Title { get; set; } = null!;
        public AccountType Type { get; set; }
        public Currency Currency { get; set; }
        public decimal CurrentBalance { get; set; }
    }
}