﻿using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.UserDtos
{
    public class CreateUserDto
    {
        [Required]
        public string Email { get; set; } = null!;
    }
}