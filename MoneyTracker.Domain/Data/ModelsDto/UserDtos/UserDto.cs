﻿using MoneyTracker.Domain.Data.Models;

namespace MoneyTracker.Domain.Data.ModelsDto.UserDtos
{
    public class UserDto
    {
        public string Email { get; set; } = null!;
        public List<Account>? Accounts { get; set; }
        public List<Category>? Categories { get; set; }
    }
}