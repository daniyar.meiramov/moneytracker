﻿using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.UserDtos
{
    public class EditUserDto
    {
        [Required]
        public Guid? Id { get; set; }
        [Required]
        public string Email { get; set; } = null!;
    }
}