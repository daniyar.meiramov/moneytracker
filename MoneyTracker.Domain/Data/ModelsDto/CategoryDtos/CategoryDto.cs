﻿using MoneyTracker.Domain.Data.Models;
using MoneyTracker.Domain.Data.Models.Enums;

namespace MoneyTracker.Domain.Data.ModelsDto.CategoryDtos
{
    public class CategoryDto
    {
        public Guid OwnerId { get; set; }
        public string OwnerEmail { get; set; } = null!;
        public bool IsParent { get; set; }
        public Guid? ParentCategoryId { get; set; }
        //public List<Transaction>? Transactions { get; set; } // а нужно ли вообще
        public string Title { get; set; } = null!;
        public CategoryType CategoryType { get; set; }
        public ExpenseType ExpenseType { get; set; }
    }
}