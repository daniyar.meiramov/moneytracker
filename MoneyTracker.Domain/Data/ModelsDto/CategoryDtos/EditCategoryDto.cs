﻿using MoneyTracker.Domain.Data.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.CategoryDtos
{
    public class EditCategoryDto
    {
        [Required]
        public Guid? Id { get; set; }
        public bool? IsParent { get; set; }
        public Guid? ParentCategoryId { get; set; }
        public string? Title { get; set; }
        public CategoryType? CategoryType { get; set; }
        public ExpenseType? ExpenseType { get; set; } 
    }
}