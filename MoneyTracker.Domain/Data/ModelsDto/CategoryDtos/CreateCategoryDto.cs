﻿using MoneyTracker.Domain.Data.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.CategoryDtos
{
    public class CreateCategoryDto
    {
        [Required]
        public Guid? OwnerId { get; set; }
        [Required]
        public bool? IsParent { get; set; }
        public Guid? ParentCategoryId { get; set; }
        [Required]
        public string Title { get; set; } = null!;
        [Required]
        public CategoryType? CategoryType { get; set; }
        [Required]
        public ExpenseType? ExpenseType { get; set; }
    }
}