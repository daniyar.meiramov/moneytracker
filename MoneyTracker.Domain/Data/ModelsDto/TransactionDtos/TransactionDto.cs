﻿using MoneyTracker.Domain.Data.Models.Enums;
using MoneyTracker.Domain.Data.Models;

namespace MoneyTracker.Domain.Data.ModelsDto.TransactionDtos
{
    public class TransactionDto
    {
        public Guid AccountId { get; set; }
        public string AccountTitle { get; set; } = null!;
        public Guid? CategoryId { get; set; }
        public string? CategoryTitle { get; set; }
        public TransactionType Type { get; set; }
        public decimal TransactionAmount { get; set; }
        public DateTime Date { get; set; }
        public string? Comment { get; set; }
    }
}