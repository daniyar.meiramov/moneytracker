﻿using MoneyTracker.Domain.Data.Models.Enums;
using MoneyTracker.Domain.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.TransactionDtos
{
    public class EditTransactionDto
    {
        [Required]
        public Guid? Id { get; set; }
        public Guid? AccountId { get; set; }
        public Guid? CategoryId { get; set; }
        public TransactionType? Type { get; set; }
        public decimal? TransactionAmount { get; set; }
        public DateTime? Date { get; set; }
        public string? Comment { get; set; }
    }
}