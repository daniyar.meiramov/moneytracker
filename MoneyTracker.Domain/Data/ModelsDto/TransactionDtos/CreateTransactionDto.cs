﻿using MoneyTracker.Domain.Data.Models.Enums;
using System.ComponentModel.DataAnnotations;

namespace MoneyTracker.Domain.Data.ModelsDto.TransactionDtos
{
    public class CreateTransactionDto
    {
        [Required]
        public Guid? AccountId { get; set; }
        public Guid? CategoryId { get; set; }
        [Required]
        public TransactionType? Type { get; set; }
        [Required]
        public decimal? TransactionAmount { get; set; }
        [Required]
        public DateTime? Date { get; set; }
        public string? Comment { get; set; }
    }
}