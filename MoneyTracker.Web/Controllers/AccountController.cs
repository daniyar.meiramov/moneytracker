﻿using Microsoft.AspNetCore.Mvc;
using MoneyTracker.Domain.Data.ModelsDto.AccountDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private IAccountService AccountService { get; }

        public AccountController(IAccountService accountService)
        {
            AccountService = accountService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AccountDto>> Get(Guid id)
        {
            try
            {
                return await AccountService.GetAccountByIdAsync(id);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateAccountDto account)
        {
            if (account == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await AccountService.AddAccountAsync(account);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(EditAccountDto account)
        {
            if (account == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await AccountService.EditAccountAsync(account);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await AccountService.DeleteAccountByIdAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}