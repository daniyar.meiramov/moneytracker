﻿using Microsoft.AspNetCore.Mvc;
using MoneyTracker.Domain.Data.ModelsDto.UserDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserService UserService { get; }

        public UserController(IUserService userService)
        {
            UserService = userService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> Get(Guid id)
        {
            try
            {
                return await UserService.GetUserByIdAsync(id);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateUserDto user)
        {
            if (user == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await UserService.RegisterUserAsync(user);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(EditUserDto user)
        {
            if (user == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await UserService.EditUserAsync(user);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await UserService.DeleteUserByIdAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}