﻿using Microsoft.AspNetCore.Mvc;
using MoneyTracker.Domain.Data.ModelsDto.TransactionDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransactionController : ControllerBase
    {
        private ITransactionService TransactionService { get; }

        public TransactionController(ITransactionService transactionService)
        {
            TransactionService = transactionService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TransactionDto>> Get(Guid id)
        {
            try
            {
                return await TransactionService.GetTransactionByIdAsync(id);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateTransactionDto transaction)
        {
            if (transaction == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await TransactionService.AddTransactionAsync(transaction);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(EditTransactionDto transaction)
        {
            if (transaction == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await TransactionService.EditTransactionAsync(transaction);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await TransactionService.DeleteTransactionByIdAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}