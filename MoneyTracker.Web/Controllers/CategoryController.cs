﻿using Microsoft.AspNetCore.Mvc;
using MoneyTracker.Domain.Data.ModelsDto.CategoryDtos;
using MoneyTracker.Domain.Services.Interfaces;

namespace MoneyTracker.Web.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase
    {
        private ICategoryService CategoryService { get; }

        public CategoryController(ICategoryService categoryService)
        {
            CategoryService = categoryService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<CategoryDto>> Get(Guid id)
        {
            try
            {
                return await CategoryService.GetCategoryByIdAsync(id);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Post(CreateCategoryDto category)
        {
            if (category == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await CategoryService.AddCategoryAsync(category);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPut]
        public async Task<ActionResult> Put(EditCategoryDto category)
        {
            if (category == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await CategoryService.EditCategoryAsync(category);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(Guid id)
        {
            try
            {
                await CategoryService.DeleteCategoryByIdAsync(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}